module HGraph.Utils where

import System.Random
import Control.Monad
import Control.Monad.State

mhead []    = Nothing
mhead (x:_) = Just x

randomN :: (Random a, RandomGen g) => a -> a -> State g a
randomN n0 n1 = do
  gen <- get
  let (r,gen') = randomR (n0,n1) gen
  put gen'
  return r

-- | Lists all subsets of `s` of size exactly `k`.
choose 0 _  = [[]]
choose _ [] = []
choose k (x:xs) = map (x:) (choose (k - 1) xs) ++ choose k xs

guessOne _ _ _ [] = Nothing
guessOne taken notTaken guess (x:xs) = 
  (taken x guess)
  `mplus`
  (guessOne taken notTaken (notTaken x guess) xs)
