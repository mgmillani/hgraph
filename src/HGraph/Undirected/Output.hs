module HGraph.Undirected.Output
  ( toDot
  )
where

import HGraph.Undirected
import Data.List

toDot g = "graph {\n  "
  ++ intercalate ";\n  " (map show $ vertices g) ++ ";\n"
  ++ intercalate ";\n  " (map (\(v,u) -> show v ++ " -- " ++ show u) $ edges g) ++ ";\n}"
