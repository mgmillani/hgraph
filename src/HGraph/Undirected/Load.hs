module HGraph.Undirected.Load
       ( loadDot
       )
where

import HGraph.Undirected
import Language.Dot.Parser as D
import Language.Dot.Utils  as D
import Language.Dot.Graph  as D
import qualified Data.Map  as M
import qualified Data.Set  as S

loadDot gr dotStr = do
  dot <- D.parse dotStr
  return $ 
    let (ns, es) = D.adjacency dot
        names = (S.toList $ S.fromList $ map getNodeName ns)
        addE (D.Edge v u _) d = addEdge (v, u) d
        getNodeName (D.Node name _) = name
    in foldr addE (foldr addVertex gr names) es
