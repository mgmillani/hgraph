module HGraph.Debugging 
       ( module HGraph.Debugging
       , sayString
       , sayErrString
       )
where

import Say
import Control.Exception
import Control.Concurrent.MVar

hasLocked :: String -> IO a -> IO a
hasLocked msg action =
  action `catches`
  [ Handler $ \exc@BlockedIndefinitelyOnMVar -> sayString ("[MVar]: " ++ msg) >> throwIO exc
  , Handler $ \exc@BlockedIndefinitelyOnSTM -> sayString ("[STM]: " ++ msg) >> throwIO exc
  ]
