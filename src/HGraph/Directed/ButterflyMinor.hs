module HGraph.Directed.ButterflyMinor
       ( isMinorOf
       , minorModel
       )
where

import HGraph.Directed

-- | Decide if `h` is a butterfly minor of `d`. 
isMinorOf h d = isJust $ minorModel h d

-- | Find a butterfly-minor model of `h` in `d`, if one exists.
minorModel h d = 
