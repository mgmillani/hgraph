module HGraph.Directed.Packing.Cycles
        ( arcMaximumI
        , maximum
        , maximumI
        )
where

import Prelude hiding (maximum)
import HGraph.Directed.Packing.Cycles.Internal
