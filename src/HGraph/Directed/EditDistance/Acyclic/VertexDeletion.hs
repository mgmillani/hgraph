module HGraph.Directed.EditDistance.Acyclic.VertexDeletion
       ( minimum
       , minimumI
       )
where

import Prelude hiding (minimum)

import HGraph.Directed.EditDistance.Acyclic.VertexDeletion.Internal
