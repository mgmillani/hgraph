module HGraph.Directed.EditDistance.Acyclic.ArcDeletion
       ( minimum
       , minimumI
       )
where

import Prelude hiding (minimum)

import HGraph.Directed.EditDistance.Acyclic.ArcDeletion.Internal
