module HGraph.Directed.Convert
       (toDot
       )
where

import Data.Graph.Directed
import qualified Language.Dot.Graph as D
import qualified Data.Map as M

toDot d vertexId vertexAttr arcAttr = (True, D.Digraph, Nothing, stmt)
where
  vStmt = [ D.NodeStatement (D.StringID $ vertexId M.! v) Nothing attr
          | v <- vertices d
          , let attr = [(D.StringID at, D.StringID vl)
                       | (at, vl) <- vertexAttr M.! v
                       , v `M.member` vertexAttr]
          ]
  aStmt = [ D.EdgeStatement [D.NodeRef (vertexId M.! v) Nothing, D.NodeRef (vertexId M.! u) Nothing attr]
          | (v,u) <- arcs d
          , let attr = [ (D.StringID at, D.StringID vl)
                       | (at, vl) <- arcAttr M.! (v,u)
                       , (v,u) `M.member` vertexAttr]
          ]
  stmt = vStmt ++ aStmt
