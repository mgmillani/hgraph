{-# LANGUAGE GADTs #-}

module Data.Graph.Directed.Basic
       ( DirectedGraph(..)
       , Adjacency(..)
       , Mutable(..)
       , Digraph
       , emptyDigraph
       )
where

import qualified Data.Map as M
import qualified Data.Set as S

type DirectedNeighborhood a = (S.Set a, S.Set a)
data Digraph a where
  Digraph :: Ord a => M.Map a (DirectedNeighborhood a) -> Digraph a

emptyDigraph :: Ord a => Digraph a
emptyDigraph = Digraph M.empty

class DirectedGraph t where
  empty :: t a -> t a
  vertices :: t a -> [a]
  numVertices :: Integral b => t a -> b
  numVertices d = fromIntegral $ length $ vertices d
  arcs :: t a -> [(a,a)]
  numArcs :: Integral b => t a -> b
  numArcs d = fromIntegral $ length $ arcs d

class Adjacency t where
  outneighbors :: t a -> a -> [a]
  inneighbors  :: t a -> a -> [a]
  outdegree :: Integral b => t a -> a -> b
  outdegree d v = fromIntegral $ length $ outneighbors d v
  indegree :: Integral b => t a -> a -> b
  indegree d v = fromIntegral $ length $ inneighbors d v
  arcExists :: t a -> (a,a) -> Bool
  metaBfs :: Ord a => t a -> a -> ([a] -> [a]) -> ([a] -> [a]) -> [a]
  metaBfs d v inFilter outFilter =
    metaBfs' S.empty (S.fromList $ (inFilter $ inneighbors d v) ++ (outFilter $ outneighbors d v))
    where
      metaBfs' visited toVisit = 
        let vs = S.toList toVisit
            newToVisit =
              (S.unions $ map
                (S.fromList . 
                  (\v -> (inFilter $ inneighbors d v) ++ (outFilter $ outneighbors d v)))
                vs
              )
              `S.difference` visited
        in if S.null newToVisit then vs else vs ++ metaBfs' (S.union (S.fromList vs) visited) newToVisit

class Mutable t where
  addVertex    :: t a -> a -> t a
  removeVertex :: t a -> a -> t a
  addArc    :: t a -> (a,a) -> t a
  removeArc :: t a -> (a,a) -> t a

instance DirectedGraph Digraph where
  empty (Digraph d) = Digraph M.empty
  numVertices (Digraph d) = fromIntegral $ M.size d
  vertices (Digraph d) = M.keys d
  arcs (Digraph d) = concatMap (\(v, (_,o)) -> [(v,u) | u <- S.toList o]) $ M.assocs d

instance Adjacency Digraph where
  outneighbors (Digraph d) v = S.toList $ snd $ d M.! v
  inneighbors  (Digraph d) v = S.toList $ fst $ d M.! v
  arcExists (Digraph d) (v,u) = u `S.member` (snd $ d M.! v)

instance Mutable Digraph where
  addVertex (Digraph d) v =
    Digraph (M.insertWith (\_ o -> o) v (S.empty, S.empty) d)
  removeVertex g@(Digraph d) v = 
      let Digraph d' = foldr (flip removeArc) g
                     (  (map (\u -> (v,u)) $ outneighbors g v)
                     ++ (map (\u -> (u,v)) $ inneighbors  g v))
      in Digraph $ M.delete v d'
            
  addArc (Digraph d) (v,u) =
    Digraph ( M.adjust (\(i,o) -> (i, S.insert u o)) v
            $ M.adjust (\(i,o) -> (S.insert v i, o)) u d)
  removeArc (Digraph d) (v,u) =
    Digraph ( M.adjust (\(i,o) -> (i, S.delete u o)) v
            $ M.adjust (\(i,o) -> (S.delete v i, o)) u d)
