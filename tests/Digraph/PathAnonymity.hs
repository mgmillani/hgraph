module Main where

import HGraph.Directed
import HGraph.Directed.PathAnonymity
import qualified HGraph.Directed.AdjacencyMap as AM
import qualified Data.Set as S

import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)

v1 = 1 :: Int

tests = TestList                         
  [ TestLabel "Path anonymity 0" $ TestCase
    ( do
      let d = addVertex v1 AM.emptyDigraph
          k = pathAnonymity d
      assertEqual "Anonymity"
        0
        k
    )
  , TestLabel "Path anonymity 1-0" $ TestCase
    ( do
      let d = foldr addArc (foldr addVertex AM.emptyDigraph [v1,2]) $ zip [1] [2]
          k = pathAnonymity d
      assertEqual "Anonymity"
        1
        k
    )
  , TestLabel "Path anonymity 1-1" $ TestCase
    ( do
      let d = foldr addArc (foldr addVertex AM.emptyDigraph [0,1,2,3,4]) $ zip [0,1,2,4] [4,4,4,3]
          k = pathAnonymity d
      assertEqual "path anonymity"
        1
        (pathPathAnonymityI d [0,4,3])
      assertEqual "digraph anonymity"
        1
        k
    )
  , TestLabel "Path anonymity 2-0" $ TestCase
    ( do
      let d = foldr addArc (foldr addVertex AM.emptyDigraph [v1,2,3,4,5]) $ zip [1,2,3,3] [3,3,4,5]
          k = pathAnonymity d
      assertEqual "Anonymity"
        2
        k
    )
  , TestLabel "Path anonymity 2-1" $ TestCase
    ( do
      let d = foldr addArc (foldr addVertex AM.emptyDigraph [0,1,2]) $ zip [0,0,1,1] [1,2,0,2]
          k = pathAnonymity d
      assertEqual "path anonymity"
        2
        (pathPathAnonymityI d [0,1,0])
      assertEqual "digraph anonymity"
        2
        k
    )
  , TestLabel "Path anonymity 4-1" $ TestCase
    ( do
      let d = foldr addArc (foldr addVertex AM.emptyDigraph [v1,2,3,4,5]) $ [(v,u) | v <- [1..5], u <- [1..5], v /= u]
          k = pathAnonymity d
      assertEqual "path anonymity"
        2
        (pathPathAnonymityI d [1,2,1])
      assertEqual "path anonymity"
        3
        (pathPathAnonymityI d [1,2,3,1])
      assertEqual "digraph anonymity"
        4
        k
    )
  , TestLabel "Path anonymity 5-0" $ TestCase
    ( do
      let d = foldr addArc (foldr addVertex AM.emptyDigraph [v1,2,3,4,5,6]) $ [(v,u) | v <- [1..6], u <- [1..6], v /= u]
          k = pathAnonymity d
      assertEqual "path anonymity"
        5
        (pathPathAnonymityI d [1,2,3,4,5,1])
      assertEqual "digraph anonymity"
        5
        k
    )
  ]

arcSet p = S.fromList $ zip p $ tail p

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
