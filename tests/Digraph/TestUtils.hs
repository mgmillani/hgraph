module TestUtils where

import qualified Data.Set as S
import HGraph.Directed
import Control.Monad
import Data.List
import Test.HUnit.Base

digraphFromArcList :: (DirectedGraph t, Mutable t, Eq a, Ord a) => t a -> [(a,a)] -> t a
digraphFromArcList d es =
  let vs = S.fromList $ concat [[v,u] | (v,u) <- es]
  in foldr addArc (foldr addVertex d $ S.toList vs) es

assertAny msg expected got = 
  when (all (/= got) expected) $ 
    assertFailure (intercalate "\n" 
      [ msg
      , "Expected one of:\n    " ++ (intercalate "\n    " $ map show expected)
      , "but got:\n    " ++ show got
      ])
