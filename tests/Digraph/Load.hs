module Main where

import HGraph.Directed
import HGraph.Directed.Load
import qualified HGraph.Directed.AdjacencyMap as AM
import qualified Data.Set as S
import Data.Either

import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)

tests = TestList                         
  [ TestLabel "Digraph 0" $ TestCase
    ( do
      let ed = loadEdgeList AM.emptyDigraph "1 0"
          Right d = ed
      assertBool "Is right"
        (isRight ed)
      assertEqual "Vertices"
        [0]
        (vertices d)
      assertEqual "Arcs"
        []
        (arcs d)
    )
  , TestLabel "Digraph 1" $ TestCase
    ( do
      let ed = loadEdgeList AM.emptyDigraph "2 1 0 1"
          Right d = ed
      assertBool "Is right"
        (isRight ed)
      assertEqual "Vertices"
        [0,1]
        (vertices d)
      assertEqual "Arcs"
        [(0,1)]
        (arcs d)
    )
  , TestLabel "Digraph 2" $ TestCase
    ( do
      let ed = loadEdgeList AM.emptyDigraph "4 2 0 1 1 0"
          Right d = ed
      assertBool "Is right"
        (isRight ed)
      assertEqual "Vertices"
        [0,1,2,3]
        (vertices d)
      assertEqual "Arcs"
        (S.fromList [(0,1), (1,0)])
        (S.fromList $ arcs d)
    )
  ]

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
