module Main where

import HGraph.Directed
import HGraph.Directed.TopologicalMinor
import qualified HGraph.Directed.AdjacencyMap as AM
import qualified Data.Map as M
import qualified Data.Set as S

import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)

tests = TestList                         
  [ TestLabel "Minor 0" $ TestCase
    ( do
      let d = addVertex 1 AM.emptyDigraph
      assertBool "Subgraph"
        (d `isMinorOf` d)
    )
  , TestLabel "Minor 1" $ TestCase
    ( do
      let d  = foldr addArc (foldr addVertex AM.emptyDigraph [0,1]) [(0,1), (1,0)]
          h1 = foldr addVertex AM.emptyDigraph [1]
          h2 = addArc (0,1) (foldr addVertex AM.emptyDigraph [0,1])
      assertBool "Minor h1"
        (h1 `isMinorOf` d)
      assertBool "Minor embedding h2"
        (isMinorEmbedding h2 d $ M.fromList [(0,0), (1,1)])
      assertBool "Minor h2"
        (h2 `isMinorOf` d)
      assertBool "Minor d"
        (d `isMinorOf` d)
    )
  ]

arcSet p = S.fromList $ zip p $ tail p

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
