module Main where

import HGraph.Directed
import qualified HGraph.Directed.EditDistance.Acyclic.VertexDeletion as VD
import qualified HGraph.Directed.EditDistance.Acyclic.ArcDeletion as AD
import qualified HGraph.Directed.AdjacencyMap as AM

import TestUtils
import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)


tests = TestList                         
  [ TestLabel "vertex deletion 1" $ TestCase
    ( do
      let d = digraphFromArcList AM.emptyDigraph [(0,1), (0,2), (1,3), (2,3)]
      assertEqual "Acyclic"
        ([], 0)
        (VD.minimum d)
      let d' = addArc (3,0) d
          (xs, k) = VD.minimum d'
      assertAny "Deletion set"
        [([0],1), ([3],1)]
        (xs,k)
    )
  , TestLabel "vertex deletion 2" $ TestCase
    ( do
      let d = digraphFromArcList AM.emptyDigraph [(0,1), (1,0), (1,2), (2,1), (0,2), (2,0)]
          (xs, k) = VD.minimum d
      print xs
      assertAny "Deletion set"
        [([0,1],2), ([0,2],2), ([1,2], 2)]
        (xs,k)
    )
  , TestLabel "arc deletion 1" $ TestCase
    ( do
      let d = digraphFromArcList AM.emptyDigraph [(0,1), (0,2), (1,3), (2,3)]
      assertEqual "Acyclic"
        ([], 0)
        (AD.minimum d)
      let d' = addArc (3,0) d
          (xs, k) = AD.minimum d'
      assertEqual "Deletion set"
        ([(3,0)], 1)
        (xs,k)
    )
  , TestLabel "arc deletion 2" $ TestCase
    ( do
      let d = digraphFromArcList AM.emptyDigraph [(0,1),(1,2),(4,0),(3,4),(2,0),(2,3)]
          (xs, k) = AD.minimum d
      assertAny "Deletion set 1"
        [([(1,2)], 1), ([(0,1)], 1)]
        (xs,k)
    )
  ]

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
