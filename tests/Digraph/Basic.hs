module Main where

import HGraph.Directed
import qualified HGraph.Directed.AdjacencyMap as AM
import qualified Data.Set as S

import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)

tests = TestList                         
  [ TestLabel "Subgraph around path 1" $ TestCase
    ( do
      let d = foldr addVertex AM.emptyDigraph [0,1,2,3,4,5]
      assertEqual "r = 0" ([]) (arcs $ subgraphAround 0 d 3)
      assertEqual "r = 1" (S.fromList [(2,3), (3,4)]) (S.fromList $ arcs $ subgraphAround 1 d 3)
      assertEqual "r = 2" (S.fromList [(2,3), (3,4), (4,5), (1,2)]) (S.fromList $ arcs $ subgraphAround 2 d 3)
      assertEqual "r = 3" (S.fromList $ arcs d) (S.fromList $ arcs $ subgraphAround 3 d 3)
    )
  , TestLabel "Digraph isomorphism" $ TestCase
    ( do
      let d0 = foldr addArc (foldr addVertex AM.emptyDigraph [0,1,2]) [(0,1), (1,2), (2,0)]
          d1 = foldr addArc (foldr addVertex AM.emptyDigraph [3,4,5]) [(3,4), (4,5), (5,3)]
      assertBool "d0 = d1" (d0 `isIsomorphicTo` d1)
    )
  ]

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
