module Main where

import HGraph.Undirected.AdjacencyMap
import qualified Data.Map as M
import Data.List

import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)

tests = TestList                         
  [ TestLabel "No edges 1" $ TestCase
    ( do
      let g = addVertex 1 emptyGraph
      assertEqual "0" 0 (degree g 1)
      assertEqual "[1]" [[1]] (connectedComponents g)
    )
  , TestLabel "No edges 2" $ TestCase
    ( do
      let g = foldr addVertex emptyGraph [1,2,3,4,5,6]
      assertEqual "1 2 3 4 5 6" [[1], [2], [3], [4], [5], [6]] (connectedComponents g)
    )
  , TestLabel "Path 1" $ TestCase
    ( do
      let g = addEdge (1,2) (foldr addVertex emptyGraph [1,2])
      assertEqual "deg(1) = 1" 1 (degree g 1)
      assertEqual "" [[1,2]] (map sort $ connectedComponents g)
      assertEqual "" [1,2] (sort $ metaBfs g 1 id)
    )
  ]

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
