{ mkDerivation, array, base, clock, containers, happy-dot, HUnit
, lib, linear, mtl, random, say, transformers
}:
mkDerivation {
  pname = "hgraph";
  version = "1.8.0.0";
  src = ./.;
  libraryHaskellDepends = [
    array base containers happy-dot linear mtl random say transformers
  ];
  testHaskellDepends = [ base containers HUnit transformers ];
  benchmarkHaskellDepends = [
    base clock containers random transformers
  ];
  description = "Tools for working on (di)graphs";
  license = lib.licenses.gpl3Only;
}
