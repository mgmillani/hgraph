with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "hgraph";
  buildInputs = [ cabal-install
    (haskellPackages.ghcWithPackages (p:
    [ p.base
      p.containers
      p.transformers
      p.HUnit
      p.happy-dot
      p.random
      p.OpenGL
      p.vector
      p.linear
      p.sdl2
      p.say
    ])
    )
  ];
}
