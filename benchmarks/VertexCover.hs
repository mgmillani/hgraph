{-# LANGUAGE BangPatterns #-}
module Main where

import HGraph.Undirected.Generator
import HGraph.Undirected.AdjacencyMap
import HGraph.Undirected.Solvers.VertexCover
import Data.List

import System.Clock
    
runBench !(g,name) = do
  start <- getTime ProcessCPUTime
  let vc = minimumVertexCover g
      !nVerts = numVertices g
      !nEdges = numEdges g
      !vcSize = length vc
  end <- getTime ProcessCPUTime
  let delta = (toNanoSecs $ diffTimeSpec end start) `div` 10^6 
  return $ ( intercalate ","
              [ show name
              , show nVerts
              , show nEdges
              , show delta
              , show vcSize
              ] ,
            delta)

instances = 
  [ (grid emptyGraph w h, "grid-" ++ show w ++ "x" ++ show h) | w <- [1..8], h <- [w..8] ]

main = do
  results <- mapM runBench instances 
  let times = map snd results
  let totalTime = sum times
  putStrLn $ intercalate "\n" $ "instance, vertices, edges, time (ms), solution" : map fst results
