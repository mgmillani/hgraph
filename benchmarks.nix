{ hgraph
}:

let benchmarks = [ "bench-vertex-cover" ];
    runBenchmark = bench: "./Setup bench " + bench + " > " + bench + ".csv";
    installBenchmark = bench: "cp dist/build/" + bench + "/" + bench + " $out/bin\n"
      + "cp " + bench + ".csv $out/results";
    postBuild = builtins.foldl' (a: b: a + b) "" (builtins.map runBenchmark benchmarks);
    installPhase = ''
      mkdir -p $out/bin
      mkdir -p $out/results
      '' + 
      builtins.foldl' (a: b: a + b) "" (builtins.map installBenchmark benchmarks);
in hgraph.overrideAttrs (old: { installPhase = installPhase ; postBuild = postBuild ; pname = "hgraph-benchmarks" ; name = "hgraph-1.0.0.0"; })
