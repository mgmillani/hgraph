module HGraph.Geometry where

instance (Num a, Num b, Num c) => Num (a,b,c) where
  (x0,y0,z0) + (x1,y1,z1) = (x0+x1,y0+y1,z0+z1)
  (x0,y0,z0) * (x1,y1,z1) = (x0*x1,y0*y1,z0*z1)
  negate (x,y,z) = (-x,-y,-z)
  abs    (x,y,z) = (abs x, abs y, abs z)
  signum (x,y,z) = (signum x, signum y, signum z)
  fromInteger i  = (fromInteger i,fromInteger i,fromInteger i)

instance (Num a, Num b) => Num (a,b) where
  (x0,y0) + (x1,y1) = (x0+x1,y0+y1)
  (x0,y0) * (x1,y1) = (x0*x1,y0*y1)
  negate (x,y) = (-x,-y)
  abs    (x,y) = (abs x, abs y)
  signum (x,y) = (signum x, signum y)
  fromInteger i  = (fromInteger i,fromInteger i)

normal (x0,y0) (x1,y1) =
  ((ux,uy),l)
  where
  dx = x1 - x0
  dy = y1 - y0
  l = sqrt $ (dx*dx + dy*dy)
  ux = dy/l
  uy = -dx/l

center (x0,y0) (x1,y1) = ((x1 + x0) / 2 , (y1 + y0)/2)

rotate alpha (x,y) = (x*ca - y*sa, x*sa + y*ca)
  where
    ca = cos alpha
    sa = sin alpha
