module HGraph.Simulation where

--data (Locatable a, Resource a) => Quadtree a

import qualified HGraph.Geometry as G

data SimpleRes = SimpleRes
  { resX :: Float
  , resY :: Float 
  , resZ :: Float
  , resWood :: Float
  , resMineral :: Float
  , resFood :: Float
  }

data (Resource a, Num b) => Actor a b = Actor (a -> Float) (b,b,b)

data Particle a =
  Particle
  { prtPos :: a
  , prtVel :: a
  }

data Slider a =
  Slider
  { slMin :: a
  , slMax :: a
  , slVal :: a
  , slNxt :: a
  }

class Resource a where
  -- | Approximates two resource by one.
  combine  :: a -> a -> a
  
class Locatable t where
  -- | Coordiates of a resource
  position   :: Num a => t a -> a
  difference :: Num a => t a -> t a -> a

class Movable t where
  velocity   :: Num a => t a -> a
  applyForce :: Num a => t a -> a -> t a
  step       :: Num a => t a -> a -> t a
  friction   :: Num a => t a -> a -> t a

{-data Tup3 a = Tup3 a a a

toTup3 (a,b,c) = (Tup3 a b c)

instance Locatable Tup3 where
  position (Tup3 x y z) = (x,y,z)
  difference (Tup3 x1 y1 z1) (Tup3 x2 y2 z2) = Tup3 (x1-x2) (y1-y2) (z1-z2)
-}
instance Locatable Slider where
  position sl = slVal sl
  difference s1 s2 = (slVal s1) - (slVal s2)

instance Movable Slider where
  velocity sl = slNxt sl
  applyForce sl f = sl{slNxt = slNxt sl + f}
  step sl t = sl{slVal = v'}
    where
      v' = slVal sl + (t * (slNxt sl - slVal sl))
  friction sl fr = sl --sl{slNxt = fr * slNxt sl}
  
instance Locatable Particle where
  position prt = prtPos prt
  difference p1 p2 = (position p1) - (position p2)

instance Movable Particle where
  velocity prt = prtVel prt
  applyForce prt fv = let pv = prtVel prt in prt{prtVel = pv+fv}
  step prt t = prt{prtPos = prtPos prt + (t * prtVel prt)}
  friction prt fr = prt{prtVel = fr * prtVel prt}

newParticle x y z = Particle{prtPos = (x,y,z), prtVel=(0,0,0)}

newSlider mn v mx = Slider{slMin = mn, slVal = v, slMax = mx, slNxt = v}

simulateVelocity obj frc t =
  friction (step obj t) (frc**t)

restrictSlider sl = sl{slVal = v', slNxt = n'}
  where
    v'
      | slVal sl < slMin sl = slMin sl
      | slVal sl > slMax sl = slMax sl
      | otherwise = slVal sl
    n'
      | slNxt sl < slMin sl = slMin sl
      | slNxt sl > slMax sl = slMax sl
      | otherwise = slNxt sl

convertSlider sl1 sl0 =
  sl0{slVal = v1 * d0/d1 + slMin sl0, slNxt = n1 * d0/d1 + slMin sl0}
  where
    v1 = slVal sl1 - slMin sl1
    n1 = slNxt sl1 - slMin sl1
    d0 = slMax sl0 - slMin sl0
    d1 = slMax sl1 - slMin sl1
    
{-instance Locatable SimpleRes where
  position (SimpleRes{resX = x, resY = y, resZ = z}) = (x,y,z)
  distance r1 r2 = distance (position r1) (position r2)

instance Floating a => Locatable (a,a,a) where
  position = id
  distance (x1,y1,z1) (x2,y2,z2) = sqrt $ (x1-x2)^2 + (y1-y2)^2 + (z1-z2)^2

-}


