{-# LANGUAGE CPP #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where

import qualified Data.ByteString               as BS
import qualified Data.Vector.Storable          as V
import qualified Graphics.Rendering.OpenGL     as GL
import qualified Graphics.Rendering.OpenGL.GLU as GLU
import qualified HGraph.Simulation             as S
import           HGraph.Undirected
import           HGraph.Undirected.AdjacencyMap
import qualified HGraph.Undirected.Layout.SpringModel as L
import           HGraph.Undirected.Load
import qualified SDL
import Control.Applicative
import Control.Monad
import Data.Int (Int32)
import Foreign.C.Types
import Linear
import SDL (($=))
import System.Environment
import System.Exit (exitFailure)
import System.IO

data Element = Circle (V2 Double) Double

data Camera = Camera (S.Particle (V2 Double)) (S.Slider Double)

data UserAction =
  UserAction
  { zoom :: Int32
  , quit :: Bool
  } 

newUserAction = UserAction{zoom = 0, quit = False}

screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (1024, 768)

main :: IO ()
main = do
  args <- getArgs
  strs <- mapM readFile args
  let eGr = foldM loadDot emptyGraph strs
  let gr =
          case eGr of
            Right g -> g
            Left _ -> emptyGraph
  let layouter = L.setup 0.001 0.0005 0.0001 0.25 gr
  print $ vertices gr
  print $ snd $ linearizeVertices gr
  print $ L.positions layouter
  SDL.initialize [SDL.InitVideo]
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do renderQuality <- SDL.get SDL.HintRenderScaleQuality
     when (renderQuality /= SDL.ScaleLinear) $
       putStrLn "Warning: Linear texture filtering not enabled!"

  window <-
    SDL.createWindow
      "hgraph-viewer"
      SDL.defaultWindow { SDL.windowInitialSize = V2 screenWidth screenHeight
                        , SDL.windowGraphicsContext = SDL.OpenGLContext SDL.defaultOpenGL
                        , SDL.windowResizable   = True
                        , SDL.windowBorder      = True}
  SDL.showWindow window

  _ <- SDL.glCreateContext(window)
  (prog, attrib) <- initResources
  let zoomSlider = S.Slider{S.slMin = -2, S.slVal = 1, S.slMax = 2, S.slNxt=1}
  let loop t0 layouter camera = do
        t1 <- SDL.ticks
        let dt = (fromIntegral $ t1 - t0)/1000
        let layouter' = L.step dt layouter
        events <- SDL.pollEvents
        let userAction = foldl parseUserAction newUserAction events
        GL.clear [GL.ColorBuffer]
        let Camera pos zm = controlCamera userAction camera
        let zm' = (S.restrictSlider $ S.simulateVelocity (S.restrictSlider zm) 1 (6*dt) )
        let camera' = Camera pos zm'
        let camera'' = Camera pos (S.convertSlider zm zoomSlider)
        draw prog (map asElement $ L.positions layouter') camera'' attrib
        SDL.glSwapWindow window
        unless (quit userAction) (loop t1 layouter' camera')

  t0 <- SDL.ticks
  loop t0 layouter
    (Camera S.Particle{S.prtPos=V2 0 0, S.prtVel=V2 0 0} (S.newSlider 0 5 10))

  SDL.destroyWindow window
  SDL.quit

asElement (_, pos) = Circle pos 0.02

parseUserAction ua event =
  case SDL.eventPayload event of
    SDL.MouseWheelEvent mwd -> parseMouseWheel ua mwd
    SDL.QuitEvent -> ua{quit = True}
    _ -> ua

parseMouseWheel ua mwd =
  let V2 ax ay = SDL.mouseWheelEventPos mwd
  in
  ua{zoom = zoom ua + ay}

controlCamera UserAction{zoom = z} (Camera pos czoom) =
  Camera pos (S.applyForce czoom (f))
  where
    z' = fromIntegral z
    f  = z'
      -- | z > 0 = (S.slNxt czoom) * z'
      -- | z < 0 = (S.slNxt czoom) / (2 * z')
      -- | otherwise = 0

initResources :: IO (GL.Program, GL.AttribLocation)
initResources = do
    -- compile vertex shader
    vs <- GL.createShader GL.VertexShader
    GL.shaderSourceBS vs $= vsSource
    GL.compileShader vs
    vsOK <- GL.get $ GL.compileStatus vs
    unless vsOK $ do
        hPutStrLn stderr "Error in vertex shader\n"
        exitFailure

    -- Do it again for the fragment shader
    fs <- GL.createShader GL.FragmentShader
    GL.shaderSourceBS fs $= fsSource
    GL.compileShader fs
    fsOK <- GL.get $ GL.compileStatus fs
    unless fsOK $ do
        hPutStrLn stderr "Error in fragment shader\n"
        exitFailure

    program <- GL.createProgram
    GL.attachShader program vs
    GL.attachShader program fs
    GL.attribLocation program "coord2d" $= GL.AttribLocation 0
    GL.linkProgram program
    linkOK <- GL.get $ GL.linkStatus program
    GL.validateProgram program
    status <- GL.get $ GL.validateStatus program
    unless (linkOK && status) $ do
        hPutStrLn stderr "GL.linkProgram error"
        plog <- GL.get $ GL.programInfoLog program
        putStrLn plog
        exitFailure
    GL.currentProgram $= Just program

    return (program, GL.AttribLocation 0)

drawElement (Circle (V2 x y) sa) = do
  GL.loadIdentity
  GL.translate (GL.Vector3 x y 0)
  GL.scale sa sa 1
  GL.drawArrays GL.TriangleFan 0 (2 + circleMeshPoints * 2)

draw :: GL.Program -> [Element] -> Camera -> GL.AttribLocation -> IO ()
draw program elements (Camera part zoom) attrib = do
    GL.clearColor $= GL.Color4 1 1 1 1
    GL.clear [GL.ColorBuffer]
    GL.loadIdentity
    let V2 zx zy = S.position part
    let z = 2 ** (S.position zoom)
    GL.matrixMode $= GL.Projection
    GL.loadIdentity
    GL.scale z z z
    GL.viewport $= (GL.Position 0 0, GL.Size (fromIntegral screenWidth) (fromIntegral screenHeight))
    GLU.lookAt (GL.Vertex3 zx zy 0) (GL.Vertex3 zx zy 2) (GL.Vector3 0 1 0)

    GL.currentProgram $= Just program
    GL.vertexAttribArray attrib $= GL.Enabled
    --GL.matrixMode $= GL.
    --GL.rotate angle (GL.Vector3 0 0 1)
    V.unsafeWith circleMesh $ \ptr ->
        GL.vertexAttribPointer attrib $=
          (GL.ToFloat, GL.VertexArrayDescriptor 2 GL.Float 0 ptr)
    GL.matrixMode $= GL.Modelview 0
    mapM_ drawElement elements
    GL.vertexAttribArray attrib $= GL.Disabled

vsSource, fsSource :: BS.ByteString
vsSource = BS.intercalate "\n"
           [ 
            "attribute vec2 coord2d; "
           , ""
           , "void main(void) { "
           , " gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex; "
           , "}"
           ]

fsSource = BS.intercalate "\n"
           [
            ""
           , "#version 120"
           , "void main(void) {"
           , "gl_FragColor = vec4(0, 0, 0, 1);"
           , "}"
           ]

circleMeshPoints = 16 :: Int32
circleMesh :: V.Vector Float
circleMesh =  V.fromList $ 0:0:concat
  [ [cos phi, sin phi]
  | x <- [0..n]
  , let i = fromIntegral x
  , let phi = i * (2 * pi) / (fromIntegral n)
  ]
  where
    n = circleMeshPoints - 1


